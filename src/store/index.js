// Set up Vuex
import Vue from 'vue';
import Vuex from 'vuex';
import {accountActions, accountMutations} from './accounts';
import {microaccountActions, microaccountMutations} from './microaccounts';
import {categoryActions, categoryMutations} from './categories';
import {treeMutations, treeGetters, calculate} from './tree';
import {historyActions, historyMutations, historyGetters} from './history';

let mutations = {
    setModal (state, modal) {
        state.modal = modal;
    },
};
Object.assign(mutations, accountMutations);
Object.assign(mutations, microaccountMutations);
Object.assign(mutations, categoryMutations);
Object.assign(mutations, treeMutations);
Object.assign(mutations, historyMutations);
let actions = {};
Object.assign(actions, accountActions);
Object.assign(actions, microaccountActions);
Object.assign(actions, categoryActions);
Object.assign(actions, historyActions);
let getters = {};
Object.assign(getters, treeGetters);
Object.assign(getters, historyGetters);

Vue.use(Vuex)
let store = new Vuex.Store({
    state: {
        accounts: {},
        microaccounts: {},
        filledMicroaccounts: "loading",
        categorys: {},
        filledCategorys: "loading",
        warnings: [],
        schedule: {},
        leftovers: 0,
        advance: 7,
        history: {},
        modal: null,
    },
    mutations,
    actions,
    getters,
});

// Startup actions

// Set up easy access to window.localStorage - automatically parsing JSON
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}
// Hook up our calculator
store.watch(state => {
    return [state.accounts, state.microaccounts, state.categorys, state.advance];
}, calculate(store), {
    deep: true,
});
// Get the current accounts
store.dispatch('getAccounts').then(() => {
    // Get the account history
    return store.dispatch('getAccountsHistory');
});
// Get the microaccounts
store.dispatch('getMicroaccounts');
// Get the categories
store.dispatch('getCategorys');


// Export the store
export default store;
