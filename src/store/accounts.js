import Vue from 'vue'

// Mutations
let accountMutations = {
    setAccounts (state, accounts) {
        state.accounts = accounts;
    },
    createAccount (state, account) {
        Vue.set(state.accounts, account.id, account);
    },
    updateAccount (state, account) {
        Vue.set(state.accounts, account.id, account);
    },
    deleteAccount (state, id) {
        Vue.delete(state.accounts, id);
    },
};

// Actions
let accountActions = {
    getAccounts (context) {
        context.commit('setAccounts', "loading");
        let storage = window.localStorage;
        let accounts = storage.getObject('accounts');
        if (accounts === null) {
            accounts = {};
            storage.setObject('accounts', accounts);
        }
        context.commit('setAccounts', accounts);
    },
    createAccount (context, account) {
        let storage = window.localStorage;
        let accounts = storage.getObject('accounts');
        if (Object.keys(accounts).indexOf('' + account.id) !== -1) {
            throw new Error("Duplicate Account ID " + account.id);
        }
        accounts[account.id] = account;
        storage.setObject('accounts', accounts);
        context.commit('createAccount', account);
        return context.dispatch('takeAccountSnapshot', account);
    },
    updateAccount (context, account) {
        let storage = window.localStorage;
        let accounts = storage.getObject('accounts');
        if (Object.keys(accounts).indexOf('' + account.id) === -1) {
            throw new Error("Updating non-existant account " + account.id);
        }
        accounts[account.id] = account;
        storage.setObject('accounts', accounts);
        context.commit('updateAccount', account);
        return context.dispatch('takeAccountSnapshot', account);
    },
    deleteAccount (context, aid) {
        let storage = window.localStorage;
        let accounts = storage.getObject('accounts');
        if (Object.keys(accounts).indexOf('' + aid) === -1) {
            throw new Error("Cant delete non-existant account " + aid);
        }
        delete accounts[aid];
        storage.setObject('accounts', accounts);
        context.commit('deleteAccount', aid);
    },
}

export {accountActions, accountMutations};
