import Vue from 'vue';
import {getSchedule, sortByProximity, getProximity} from './schedule';

let calculate = store => { return state => {
    let [accounts, microaccounts, categorys, advance] = state;
    if (
        accounts === "loading" ||
        accounts === undefined ||
        microaccounts === "loading" ||
        microaccounts === undefined ||
        categorys === "loading" ||
        categorys === undefined
        ) {
        return "loading";
    }
    store.commit('setSchedule', "loading");
    store.commit('setWarnings', "loading");
    microaccounts = clone(microaccounts);
    for (let mid in microaccounts) {
        microaccounts[mid].balance = 0;
        microaccounts[mid].full = false;
    }
    categorys = clone(categorys);
    let account_list = Object.keys(accounts).map(a => {
        return accounts[a];
    });
    let errors = [];
    account_list.sort((a,b) => {
        return a.mobility - b.mobility;
    });
    let accounts_sorted_by_interest = account_list.slice();
    accounts_sorted_by_interest.sort((a,b) => {
        return a.interest - b.interest ;
    });
    let max_days_in_advance = Math.floor(
        (Math.ceil(
            Math.max(...account_list.map(a => {
                return a.mobility;
            }))
        )) + advance
    );
    let schedule = getSchedule(microaccounts, max_days_in_advance);
    schedule.sort((a,b) => {
        return (a[0].target * (a[0].vital?0.001:1)) - (b[0].target * (b[0].vital?0.001:1))
    });
    let drainable_accounts = account_list.map(a => {
        return a.balance;
    });
    // Setup the audit log
    let audit_log = {};
    for (var i = 0; i < account_list.length; i++) {
        audit_log[account_list[i].id] = [];
    }

    // Put scheduled money away
    for (let item of schedule) {
        let [bill, days_away] = item;
        let to_drain = bill.target;
        for (let i = 0; i < drainable_accounts.length; i++) {
            let bal = drainable_accounts[i];
            if (bal === 0) {
                continue;
            }
            let acc = account_list[i];
            let drain_this = Math.min(bal, to_drain);
            drainable_accounts[i] -= drain_this;
            to_drain -= drain_this;
            // Put this in the audit log
            let to_account = account_list[0];
            let etype = "move";
            if (acc === account_list[0]) {
                if (bill.silent) {
                    etype = "silent";
                } else {
                    etype = "pay";
                }
                to_account = null;
            } else {
                for (let account of accounts_sorted_by_interest) {
                    if (days_away - account.mobility > advance) {
                        to_account = account;
                        break;
                    }
                }
            }
            audit_log[acc.id].push([bill, acc, to_account, drain_this, etype, days_away - acc.mobility, days_away]);
            if (to_drain === 0) {
                break;
            }
        }
        if (to_drain !== 0) {
            console.log("Oh no! We don't have enough money to pay " + item.name);
        }
    }
    // Put things in the far future away
    // - Into accounts with highest interest
    for (let micro of sortByProximity(microaccounts)) {
        if (getProximity(micro) > advance) {
            let to_drain = micro.target;
            for (let i = 0; i < drainable_accounts.length; i++) {
                let bal = drainable_accounts[i];
                if (bal === 0) {
                    continue;
                }
                let acc = account_list[i];
                let drain_this = Math.min(bal, to_drain);
                drainable_accounts[i] -= drain_this;
                to_drain -= drain_this;
                // Put this in the audit log
                let to_account = account_list[account_list.length-1];
                let etype = "move";
                if (acc === to_account) {
                    etype = "silent";
                    to_account = null;
                }
                audit_log[acc.id].push([micro, acc, to_account, drain_this, etype, 0, getProximity(micro)]);
                if (to_drain === 0) {
                    break;
                }
            }
        }
    }

    let remainder = drainable_accounts.reduce((sum,a) => {
        return sum + a;
    }, 0);

    // Replay the events that occured in the last advance period onto the microaccounts
    let new_audit_log = {}; // In terms of due now!
    let unpayables = {};
    drainable_accounts = account_list.map(a => {
        return a.balance;
    });
    let aids = Object.keys(audit_log);
    for (let aid of  aids) {
        let auditEvents = audit_log[aid];
        for (let e of auditEvents) {
            let [micro,account,to_account,amount,etype,due,pmt_due] = e;
            if (due < 0) {
                if (Object.keys(unpayables).indexOf("" + micro.id) === -1) {
                    unpayables[micro.id] = [];
                }
                unpayables[micro.id].push(e);
            } else if (due <= advance) {
                if (new_audit_log[parseInt(due)] === undefined) {
                    new_audit_log[parseInt(due)] = [];
                }
                new_audit_log[parseInt(due)].push({
                    from_account_id: aid,
                    to_account_id: (to_account?to_account.id:null),
                    microaccount_id: micro.id,
                    amount: amount,
                    type: etype,
                    due: parseInt(due),
                    payment_due: pmt_due,
                });
                micro.balance += amount;
                drainable_accounts[account_list.indexOf(account)] -= amount;
                if (micro.balance >= micro.target) {
                    micro.full = true;
                }
            }
        }
    }

    for (let mid in unpayables) {
        let m = microaccounts[mid];
        let c = unpayables[mid].length;
        let nearest = Math.min(...unpayables[mid].map(e => e[6]))
        let dates = unpayables[mid].map(e => {
            let timestamp = new Date().getTime() + (86400000 * e[6]);
            let date = new Date(timestamp);
            let day = date.getDate();
            let month = date.getMonth() + 1;
            day = day > 9 ? day : "0" + day;
            month = month > 9 ? month : "0" + month;
            return day + "/" + month + "/" + date.getFullYear();
        });
        errors.push({
            type: (m.vital?'error':'note'),
            message: "There is no way to pay for " + m.name + " in " + nearest + " days" + (c===1?'.':', or the next ' + c + ' times after that.'),
            details: "Based on your current account balances, you will not be able to pay for " + m.name + " on the following date" + (c===1?'':'s') + ': ' + dates.join(', ') + '.',
        });
    }

    // This is money that is earmarked for payments within the period we looked
    // in, but that is not used in the next `advance` days
    let invisible_earmarked = drainable_accounts.reduce((sum, a) => {return sum+a}, 0) - remainder;

    // Add a note about there being leftovers
    if (remainder) {
        errors.push({
            type: 'note',
            message: "You have $" + remainder.toFixed(2) + " left over.",
            details: "Leftover money is a good thing! But to get the most out of this software you should probably create another savings account, or increase the target balance of an existing one.",
        });
    }

    // Idiot check!
    let account_total = account_list.reduce((sum,a) => {
        return sum + a.balance;
    }, 0);
    let micro_total = remainder + invisible_earmarked;
    let mids = Object.keys(microaccounts);
    for (let mid of mids) {
        micro_total += microaccounts[mid].balance;
    }
    if (account_total !== micro_total) {
        console.log("Uh oh! We've come unstuck somewhere!")
        throw new Error("Amount has changed from " + account_total + " to " + micro_total);
    }

    // Update Category full and balance props
    let tree = treeGetters["tree"]({
        accounts: accounts,
        filledMicroaccounts: microaccounts,
        filledCategorys: categorys,
    });
    let update_state = node => {
        // Update children
        for (let i = 0; i < node.children.length; i++) {
            if (node.children[i].type === "Category") {
                update_state(node.children[i]);
            }
        }
        // Update self
        let any_not_full = false;
        let total_balance = 0;
        for (let i = 0; i < node.children.length; i++) {
            if (!node.children[i].full) {
                any_not_full = true;
            }
            total_balance = node.children[i].balance;
        }
        categorys[node.id].full = !any_not_full;
        categorys[node.id].balance = total_balance;
    }
    if (tree) {
        update_state(tree);
    }

    // Set the leftovers, warnings, schedule
    store.commit('setWarnings', errors);
    store.commit('setLeftovers', remainder);
    store.commit('setSchedule', new_audit_log);
    store.commit('setFilledMicroaccounts', microaccounts);
    store.commit('setFilledCategorys', categorys);
}}

let clone = obj => {
    let newObj = {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (typeof obj[key] === 'object') {
                let newVal = {};
                for (let valKey in obj[key]) {
                    if (obj[key].hasOwnProperty(valKey)) {
                        newVal[valKey] = obj[key][valKey];
                    }
                }
                newObj[key] = newVal;
            }
        }
    }
    return newObj;
}

// Getters
let treeGetters = {
    tree (state) {
        // Check if we have everything we need
        if (
            state.accounts === "loading" ||
            state.accounts === undefined ||
            state.filledMicroaccounts === "loading" ||
            state.filledMicroaccounts === undefined ||
            state.filledCategorys === "loading" ||
            state.filledCategorys === undefined
            ) {
            return "loading";
        }
        // Make the tree
        let root;
        let cids = Object.keys(state.filledCategorys);
        // Make sure all children are empty
        for (let cid of cids) {
            state.filledCategorys[cid].children = [];
        }
        for (var i = 0; i < cids.length; i++) {
            let cid = cids[i];
            let cat = state.filledCategorys[cid]
            if (cat.parent === -1) {
                root = cat;
            } else {
                state.filledCategorys[cat.parent].children.push(cat);
            }
        }
        if (!root) {
            return null;
        }
        // Add the microaccounts
        let maids = Object.keys(state.filledMicroaccounts);
        for (var i = 0; i < maids.length; i++) {
            let micro = state.filledMicroaccounts[maids[i]]
            state.filledCategorys[micro.parent].children.push(micro);
        }

        // Bind some helpers to the tree
        root.get_by_id = function (id, type) {
            let nodes = this.flatten();
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].node.type === type && nodes[i].node.id === id) {
                    return nodes[i];
                }
            }
        };
        root.flatten = function () {
            let _flatten = function(node, path) {
                let nodes = [{
                    node: node,
                    path,
                }];
                if (node.children) {
                    for (var i = 0; i < node.children.length; i++) {
                        nodes = [...nodes, ..._flatten(node.children[i], [...path, i])]
                    }
                }
                return nodes;
            }
            return _flatten(this, []);
        };

        return root;
    },
}

// Mutations, including some misc ones set by the watcher
let treeMutations = {
    setWarnings (state, warnings) {
        state.warnings = warnings;
    },
    setSchedule (state, schedule) {
        state.schedule = schedule;
    },
    setLeftovers (state, leftovers) {
        state.leftovers = leftovers;
    },
    setAdvance (state, advance) {
        state.advance = advance;
    },
};

// Actions
let treeActions = {
    getTree (context) {
        context.commit('setTree', "loading");
        context.commit('setSchedule', "loading");
        context.commit('setWarnings', "loading");
        context.commit('updateAllAccounts', "loading");
        context.commit('setLeftovers', 0);
        let advance = context.state.advance;
        let options = {
            headers: {
                'Accept': 'application/json',
            },
        }
        return fetch("/tree?advance=" + advance, options)
        .then(r => r.json())
        .then(resp => {
            if (resp.ok) {
                // Bind some helpers to the tree
                resp.result.tree.get_by_id = function (id, type) {
                    let nodes = this.flatten();
                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].node.type === type && nodes[i].node.id === id) {
                            return nodes[i];
                        }
                    }
                };
                resp.result.tree.flatten = function () {
                    let _flatten = function(node, path) {
                        let nodes = [{
                            node: node,
                            path,
                        }];
                        if (node.children) {
                            for (var i = 0; i < node.children.length; i++) {
                                nodes = [...nodes, ..._flatten(node.children[i], [...path, i])]
                            }
                        }
                        return nodes;
                    }
                    return _flatten(this, []);

                };
                context.commit('setTree', resp.result.tree);
                context.commit('updateAllAccounts', resp.result.accounts);
                context.commit('setSchedule', resp.result.schedule);
                context.commit('setWarnings', resp.result.warnings);
                context.commit('setLeftovers', resp.result.leftovers);
                return;
            } else {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    updateCategory (context, category) {
        // Find the category
        let cid = category.id;
        let prior = context.getters.tree.get_by_id(category.id, "Category");
        context.commit('updateCategory', {
            path: prior.path,
            category: Object.assign(prior.node, category),
        });
        // Make _sure_ that parent is present
        if (category.parent === undefined){
            let parent = context.getters.tree;
            let parentPath = [...prior.path];
            parentPath.pop();
            parentPath.map((index, i) => {
                parent = parent.children[index];
            });
            category.parent = parent.id;
        }
        let options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(category),
        };
        fetch("/category/" + category.id, options)
        .then(r => r.json())
        .then(resp => {
            if (!resp.ok) {
                // Rollback
                context.commit('updateCategory', {
                    path: prior.path,
                    category: prior.node,
                });
                throw new Error(JSON.stringify(resp));
            } else {
                context.commit('updateCategory', {
                    path: prior.path,
                    category: resp.result.Category,
                });
            }
        });
    },
    createCategory (context, category) {
        let options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(category),
        };
        fetch("/category", options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    deleteCategory (context, id) {
        let options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
            },
        };
        fetch("/category/" + id, options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    updateMicroaccount (context, micro) {
        let options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(micro),
        };
        fetch("/microaccount/" + micro.id, options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    createMicroaccount (context, micro) {
        let options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(micro),
        };
        fetch("/microaccount", options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    deleteMicroaccount (context, id) {
        let options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
            },
        };
        fetch("/microaccount/" + id, options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    updateAccount (context, account) {
        let options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(account),
        };
        fetch("/account/" + account.id, options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    createAccount ({commit, dispatch}, account) {
        let options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(account),
        };
        fetch("/account", options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
    deleteAccount ({commit, dispatch}, id) {
        let options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
            },
        };
        fetch("/account/" + id, options)
        .then(r => r.json())
        .then(resp => {
            // Reload everything from the server
            context.dispatch('getTree');
            // It there was an error, throw it
            if (!resp.ok) {
                throw new Error(JSON.stringify(resp));
            }
        });
    },
}

export {calculate, treeMutations, treeGetters};
