import Vue from 'vue';
import Moment from 'moment';

// Mutations
let historyMutations = {
    setAccountHistory (state, details) {
        Vue.set(state.history, details.aid, details.history);
    },
};

// Getters
let historyGetters = {
    combinedLabels (state) {
        let history = state.history;
        let today = Moment();
        let earliest = Moment();
        for (let aid in history) {
            if (history.hasOwnProperty(aid)) {
                for (let point of history[aid]) {
                    if (Moment(point.date).diff(earliest) < 0) {
                        earliest = Moment(point.date);
                    }
                }
            }
        }
        let labels = [];
        let length = Math.ceil(Moment.duration(today.diff(earliest)).asDays());
        for (let i = 0; i < length; i++) {
            labels.push(Moment(earliest).add(i, 'days').format('YYYY-MM-DD'));
        }
        return labels;
    },
    combinedHistory (state, getters) {
        let dates = getters.combinedLabels;
        let aids = Object.keys(state.history);
        return dates.map(date => {
            let total = 0;
            for (let aid of aids) {
                // We want to find either this date, or the most recent
                // previous one.
                // To do this we find the first date that is larger than
                // our target, then get the date one before that.
                let index = state.history[aid].findIndex(p => {
                    return Moment(p.date).diff(Moment(date)) > 0;
                }) - 1;
                // Edge case - there are no dates larger than given
                if (index === -2) {
                    if (state.history[aid].length) {
                        total +=  state.history[aid][state.history[aid].length - 1].balance;
                        continue;
                    }
                }
                // Edge case - the found date was the first one
                if (index === -1) {
                    continue;
                }
                total +=  state.history[aid][index].balance;
            }
            return {
                x: date,
                y: total,
            }
        })
    },
};

// Actions
let historyActions = {
    getAccountsHistory (context) {
        if (typeof context.state.accounts === "object") {
            for (var aid in context.state.accounts) {
                if (context.state.accounts.hasOwnProperty(aid)) {
                    context.dispatch('getAccountHistory', aid);
                }
            }
        }
    },
    getAccountHistory (context, aid) {
        let storage = window.localStorage;
        let history = storage.getObject('history_' + aid);
        if (history === null) {
            history = [];
            storage.setObject('history_' + aid, history);
        }
        context.commit('setAccountHistory', {
            aid,
            history,
        });
    },
    takeAccountSnapshot (context, account) {
        let storage = window.localStorage;
        // Create the history is needed
        let history = storage.getObject('history_' + account.id);
        if (history === null) {
            history = [];
        }
        if (history.length &&
            Moment.duration(Moment().diff(Moment(history[history.length-1].date))).asDays() < 1
        ) {
            // Last snapshot was today, so edit it
            history[history.length - 1].balance = account.balance;
        } else {
            // New snapshot for today
            history.push({
                date: Moment().format("YYYY-MM-DD"),
                balance: account.balance,
            });
        }
        storage.setObject('history_' + account.id, history);
        context.commit('setAccountHistory', {
            aid: account.id,
            history,
        });
    },
}

export {historyActions, historyMutations, historyGetters};
