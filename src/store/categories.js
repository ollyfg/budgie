import Vue from 'vue'

// Mutations
let categoryMutations = {
    setCategorys (state, categorys) {
        state.categorys = categorys;
    },
    setFilledCategorys (state, categorys) {
        state.filledCategorys = categorys;
    },
    createCategory (state, category) {
        Vue.set(state.categorys, category.id, category);
    },
    updateCategory (state, category) {
        Vue.set(state.categorys, category.id, category);
    },
    deleteCategory (state, id) {
        Vue.delete(state.categorys, id);
    },
};

// Actions
let categoryActions = {
    getCategorys (context) {
        context.commit('setCategorys', "loading");
        let storage = window.localStorage;
        let categorys = storage.getObject('categorys');
        if (categorys === null) {
            categorys = {};
            storage.setObject('categorys', categorys);
        }
        context.commit('setCategorys', categorys);
    },
    createCategory (context, category) {
        let storage = window.localStorage;
        let categorys = storage.getObject('categorys');
        let nextId = Math.max(0, ...Object.keys(categorys).map(x => {
            return parseInt(x, 10)})) + 1;
        if (Object.keys(categorys).indexOf('' + nextId) !== -1) {
            throw new Error("Duplicate Category ID " + nextId);
        }
        category.id = nextId;
        categorys[nextId] = category;
        storage.setObject('categorys', categorys);
        context.commit('createCategory', category);
    },
    updateCategory (context, category) {
        let storage = window.localStorage;
        let categorys = storage.getObject('categorys');
        if (Object.keys(category).indexOf('' + category.id) === -1) {
            throw new Error("Updating non-existant Category " + category.id);
        }
        categorys[category.id] = category;
        storage.setObject('categorys', categorys);
        context.commit('createCategory', category);
    },
    deleteCategory (context, cid) {
        let storage = window.localStorage;
        let categorys = storage.getObject('categorys');
        if (Object.keys(categorys).indexOf(cid) === -1) {
            throw new Error("Cant delete non-existant category " + cid);
        }
        delete categorys[cid];
        storage.setObject('categorys', categorys);
        context.commit('deleteCategory', cid);
    },
}

export {categoryActions, categoryMutations};
