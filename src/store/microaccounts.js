import Vue from 'vue'

// Mutations
let microaccountMutations = {
    setFilledMicroaccounts (state, microaccounts) {
        state.filledMicroaccounts = microaccounts;
    },
    setMicroaccounts (state, microaccounts) {
        state.microaccounts = microaccounts;
    },
    createMicroaccount (state, microaccount) {
        Vue.set(state.microaccounts, microaccount.id, microaccount);
    },
    updateMicroaccount (state, microaccount) {
        Vue.set(state.microaccounts, microaccount.id, microaccount);
    },
    deleteMicroaccount (state, id) {
        Vue.delete(state.microaccounts, id);
    },
};

// Actions
let microaccountActions = {
    getMicroaccounts (context) {
        context.commit('setMicroaccounts', "loading");
        let storage = window.localStorage;
        let microaccounts = storage.getObject('microaccounts');
        if (microaccounts === null) {
            microaccounts = {};
            storage.setObject('microaccounts', microaccounts);
        }
        context.commit('setMicroaccounts', microaccounts);
    },
    createMicroaccount (context, microaccount) {
        let storage = window.localStorage;
        let microaccounts = storage.getObject('microaccounts');
        let nextId = Math.max(0, ...Object.keys(microaccounts).map(x => {
            return parseInt(x, 10)
        })) + 1;
        if (Object.keys(microaccounts).indexOf('' + nextId) !== -1) {
            throw new Error("Duplicate Microaccount ID " + nextId);
        }
        microaccount.id = nextId;
        microaccounts[nextId] = microaccount;
        storage.setObject('microaccounts', microaccounts);
        context.commit('createMicroaccount', microaccount);
    },
    updateMicroaccount (context, microaccount) {
        let storage = window.localStorage;
        let microaccounts = storage.getObject('microaccounts');
        if (Object.keys(microaccounts).indexOf('' + microaccount.id) === -1) {
            throw new Error("Can't update non-existant Microaccount " + microaccount.id);
        }
        microaccounts[microaccount.id] = microaccount;
        storage.setObject('microaccounts', microaccounts);
        context.commit('updateMicroaccount', microaccount);
    },
    deleteMicroaccount (context, aid) {
        let storage = window.localStorage;
        let microaccounts = storage.getObject('microaccounts');
        if (Object.keys(microaccounts).indexOf('' + aid) === -1) {
            throw new Error("Cant delete non-existant microaccount " + aid);
        }
        delete microaccounts[aid];
        storage.setObject('microaccounts', microaccounts);
        context.commit('deleteMicroaccount', aid);
    },
}

export {microaccountActions, microaccountMutations};
