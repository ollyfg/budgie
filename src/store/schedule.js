// Helpers to do with schedules
import Moment from 'moment';

let getSchedule = (micros, days) => {
    let today = Moment();
    let schedule = [];
    for (let i = 0; i < days; i++) {
        let day = Moment(today).add(i, 'days');
        let maids = Object.keys(micros);
        for (let j = 0; j < maids.length; j++) {
            let maid = maids[j]
            let scheduleFn = fromPattern(micros[maid].schedule);
            if (scheduleFn(day) === 0) {
                schedule.push([micros[maid], i]);
            }
        }
    }
    return schedule;
}

let sortByProximity = micros => {
    let arr = [];
    let maids = Object.keys(micros);
    let today = Moment();
    for (var i = 0; i < maids.length; i++) {
        arr.push(micros[maids[i]]);
    }
    arr.sort((a,b) => {
        return fromPattern(a.schedule)(today) - fromPattern(b.schedule)(today);
    });
    return arr;
}

let getProximity = micro => {
    return fromPattern(micro.schedule)(Moment());
}

let Weekly = day => {
    return today => {
        let days_to_go = day - today.day();

        if (days_to_go < 0) {
            days_to_go += 7;
        }
        return days_to_go;
    }
}

let Biweekly = date => {
    return today => {
        let diff = (Math.floor(Moment.duration(today.diff(date)).asDays() / 14) + 1) * 14;
        let next_occurance = Moment(date).add(diff, 'days');
        return Math.floor((Moment.duration(today.diff(next_occurance)).asDays() * -1) % 14);
    }
}

let Monthly = date => {
    return today => {
        let days_to_go = date - today.date()
        if (days_to_go < 0) {
            let date_next_month = Moment({
                year: today.year(),
                month: today.month() + 1,
                day: date,
            });
            days_to_go = Math.ceil(Moment.duration(date_next_month.diff(today)).asDays());
        }
        return days_to_go;
    }
}

let Bimonthly = (date, odd) => {
    return today => {
        let month = today.month();
        if ((odd && (month % 2) === 0) || (!odd && (month % 2) === 1)) {
            month += 1;
        }
        month += 1; // Moment.JS uses 0-indexed months
        let next_occurance = Moment({
            year: today.year(),
            month: month,
            day: date,
        });
        if (next_occurance.diff(today) < 0) {
            next_occurance = Moment({
                year: today.year(),
                month: month + 2,
                day: date,
            });
        }
        return Math.floor(Moment.duration(next_occurance.diff(today)).asDays());
    }
}

let Yearly = date => {
    return today => {
        let possibility1 = Moment({
            year: today.year(),
            month: date.month(),
            day: date.date(),
        });
        let possibility2 = Moment({
            year: today.year() + 1,
            month: date.month(),
            day: date.date(),
        });
        if (possibility1.diff(today) > 0) {
            return Math.floor(Moment.duration(possibility1.diff(today)).asDays());
        } else {
            return Math.floor(Moment.duration(possibility2.diff(today)).asDays());
        }
    }
}

let Daily = () => {
    return today => {
        return 0;
    }
}

let On = date => {
    return today => {
        return Math.floor(Moment.duration(date.diff(today)).asDays());
    }
}

let fromPattern = pattern => {
    let parts = pattern.split('|');
    let date;
    switch (parts[0]) {
        case 'd':
            return Daily();
        case 'w':
            return Weekly(parseInt(parts[1]));
        case '2w':
            date = Moment(parseInt(parts[1]) * 1000);
            return Biweekly(date);
        case 'm':
            return Monthly(parseInt(parts[1]));
        case '2m':
            return Bimonthly(parseInt(parts[1]), parseInt(parts[2]));
        case 'y':
            date = Moment(parseInt(parts[1]) * 1000);
            return Yearly(date);
        case 'on':
            date = Moment(parseInt(parts[1]) * 1000);
            return Weekly(date);
    }
}

export {getSchedule, sortByProximity, getProximity};
